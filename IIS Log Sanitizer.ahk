; IIS Log Sanitizer
; Removes commented lines from Microsoft IIS Logs to prepare them for importing into a SQL database.
; This will act upon an entire directory of .log files, removing the commented lines and renaming the
; file to s_{OriginalFileName). After the various files have been cleaned, they're combined into a
; single file for easier import into a SQL database.



SetWorkingDir %A_ScriptDir%
combineFile = ImportFile.log

; Set up an empty file list to manage all the files
FileList := ""

; Read through the file list, specifically looking for only log files.
Loop, Files, *.log
    FileList .= A_LoopFileName "`n"

; Parse the file list and act upon the files within to remove the comments with # in them.
Loop, Parse, FileList, `n
    Loop, read, %A_LoopField%, s_%A_LoopField%
    {
        if not InStr(A_LoopReadLine, "#Software") and not InStr(A_LoopReadLine, "#Version") and not InStr(A_LoopReadLine, "#Date: ") and not InStr(A_LoopReadLine, "#Fields")
            FileAppend, %A_LoopReadLine%`n
    }

; Now that the comments are removed, combine the sanitized files into a single import file.
Loop, %A_ScriptDir%\s_*.log

{
  FileRead, aFileContents, %A_LoopFileFullPath% 
  FileAppend, %aFileContents%, %combineFile% 
}

MsgBox, 0, IIS Log Sanitizer, Logs sanitized. Import file is ready.