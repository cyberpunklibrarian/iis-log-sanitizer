# IIS Log Sanitizer

This is a simple script, written in AutoHotkey, that removes comments from an IIS log. These comments can appear throughout the log, especially if IIS was restarted at some point during the day. In this case, IIS Log Sanitizer removes:

* The #Software comment
* The #Version comment
* The #Date comment
* The #Fields comment

You can run this script in a directory full of IIS logs and it will roll through the folder and sanitize all of them. After the comments are removed, the resulting sanitized logs are combined into a single file called ImportFile.log. This makes it easier to import the log files, en masse, into a SQL database. 

## Importing Into a Database

Set up your IIS log database with a query like this:

```
DROP TABLE IF EXISTS dbo.IISLOG
CREATE TABLE dbo.IISLOG (
[DATE] [DATE] NULL,
[TIME] [TIME] NULL,
[s-ip] [VARCHAR] (48) NULL,
[cs-method] [VARCHAR] (8) NULL,
[cs-uri-stem] [VARCHAR] (255) NULL,
[cs-uri-query] [VARCHAR] (2048) NULL,
[s-port] [VARCHAR] (4) NULL,
[s-username] [VARCHAR] (256) NULL,
[c-ip] [VARCHAR] (48) NULL,
[cs(User-Agent)] [VARCHAR] (1024) NULL,
[cs(Referer)] [VARCHAR] (4096) NULL,
[sc-STATUS] [INT] NULL,
[sc-substatus] [INT] NULL,
[sc-win32-STATUS] [BIGINT] NULL,
[time-taken] [INT] NULL,
INDEX cci CLUSTERED COLUMNSTORE
)
```

After that, you can import the combined log into the database:

```
BULK INSERT dbo.IISLOG
FROM 'C:\Temp\ImportFile.log'
WITH (
FIRSTROW = 1,
FIELDTERMINATOR = ' ',
ROWTERMINATOR = '\n'
)
```

## Logo Credits

* [Spray Bottle](https://thenounproject.com/search/?creator=3555590&q=spray&i=2451657) by Vectors Point from the Noun Project

* [log](https://thenounproject.com/search/?q=log&i=2026087) by Kiran Shastry from the Noun Project